package eu.codingtemple.trackme.app;

import android.app.Application;
import android.util.Log;
import eu.codingtemple.trackme.SinkStateListener;
import eu.codingtemple.trackme.TrackMe;
import eu.codingtemple.trackme.event.TargetEvent;
import eu.codingtemple.trackme.sink.ConsentStorage;
import eu.codingtemple.trackme.sink.Hashable;
import org.jetbrains.annotations.NotNull;

public class MainApplication extends Application {

    private TrackMe trackMe;

    @Override
    public void onCreate() {
        super.onCreate();

        ConsentStorage storage = new SampleStorage();
        SampleSink sink1 = new SampleSink();
        SampleSharedStorageSink sink2 = new SampleSharedStorageSink(storage);

        SinkStateListener listener = new SinkStateListener() {
            @Override
            public void onSinkInitialized(@NotNull Hashable sinkId) {
                Log.i("TEST", "onSinkInitialized " + sinkId);

                trackMe.start();

                trackMe.setConsentTrue(sink2.getId());
            }

            @Override
            public void onSinkInitError(@NotNull Throwable throwable) {
                Log.i("TEST", "onSinkInitError ");
            }

            @Override
            public void onSinkStarted(@NotNull Hashable sinkId) {
                // This event will be logged twice to sink1 becase we trigger it on sink start (both sinks are started at some point) but event targets only sink1
                TargetEvent event = new TargetEvent.Builder("testEventId")
                        .attribute("someCustomAttributeKey", "value")
                        .sink(sink1.getId())
                        .build();

                getTrackMeInstance().log(event);
            }

            @Override
            public void onSinkFinished(@NotNull Hashable sinkId) {
                Log.i("TEST", "onSinkInitError " + sinkId);
            }

            @Override
            public void onAllSinksInitialized() {
                Log.i("TEST", "onAllSinksInitialized ");
            }

            @Override
            public void onError(@NotNull Throwable throwable) {
                Log.i("TEST", "onError ");
            }
        };

        trackMe = new TrackMe.Builder()
                .withBlocking(false)
                .withConsentOverride(false, false)
                .withSinkListener(listener)
                .withSink(sink1)
                .withSink(sink2)
                .build();

        trackMe.initialize(this);
    }

    public TrackMe getTrackMeInstance() {
        return trackMe;
    }
}
